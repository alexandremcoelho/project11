SELECT 
	*
FROM
	enderecos e 

	-- segundo
	
SELECT 
	*
FROM
	enderecos e
JOIN 
	usuarios u
	ON e.id = u.endereco_id
ORDER BY
	u.endereco_id;

--terceiro

SELECT 
	rs , u 
FROM
	redes_sociais rs
JOIN 
	usuario_rede_sociais urs
	ON urs.rede_social_id = rs.id
JOIN 
	 usuarios u 
	ON urs.usuario_id  = u.id;

-- quarto

SELECT 
	rs, u, e
FROM
	redes_sociais rs
JOIN 
	usuario_rede_sociais urs
	ON urs.rede_social_id = rs.id
JOIN 
	 usuarios u 
	ON urs.usuario_id  = u.id
JOIN 
	enderecos e 
	ON u.endereco_id = e.id;

-- quinto

SELECT 
	rs.nome, u.nome, u.email, e.cidade 
FROM
	redes_sociais rs
JOIN 
	usuario_rede_sociais urs
	ON urs.rede_social_id = rs.id
JOIN 
	 usuarios u 
	ON urs.usuario_id  = u.id
JOIN 
	enderecos e 
	ON u.endereco_id = e.id;

-- sexto


SELECT 
	rs.nome, u.nome, u.email, e.cidade 
FROM
	redes_sociais rs
JOIN 
	usuario_rede_sociais urs
	ON urs.rede_social_id = rs.id
JOIN 
	 usuarios u 
	ON urs.usuario_id  = u.id
JOIN 
	enderecos e 
	ON u.endereco_id = e.id
WHERE
	rs.nome = 'youtube';



--setimo

SELECT 
	rs.nome, u.nome, u.email, e.cidade 
FROM
	redes_sociais rs
JOIN 
	usuario_rede_sociais urs
	ON urs.rede_social_id = rs.id
JOIN 
	 usuarios u 
	ON urs.usuario_id  = u.id
JOIN 
	enderecos e 
	ON u.endereco_id = e.id
WHERE
	rs.nome = 'Instagram';

--oitavo

SELECT 
	rs.nome, u.nome, u.email, e.cidade 
FROM
	redes_sociais rs
JOIN 
	usuario_rede_sociais urs
	ON urs.rede_social_id = rs.id
JOIN 
	 usuarios u 
	ON urs.usuario_id  = u.id
JOIN 
	enderecos e 
	ON u.endereco_id = e.id
WHERE
	rs.nome = 'Facebook';

--nono

SELECT 
	rs.nome, u.nome, u.email, e.cidade 
FROM
	redes_sociais rs
JOIN 
	usuario_rede_sociais urs
	ON urs.rede_social_id = rs.id
JOIN 
	 usuarios u 
	ON urs.usuario_id  = u.id
JOIN 
	enderecos e 
	ON u.endereco_id = e.id
WHERE
	rs.nome = 'TikTok';

--decimo

SELECT 
	rs.nome, u.nome, u.email, e.cidade 
FROM
	redes_sociais rs
JOIN 
	usuario_rede_sociais urs
	ON urs.rede_social_id = rs.id
JOIN 
	 usuarios u 
	ON urs.usuario_id  = u.id
JOIN 
	enderecos e 
	ON u.endereco_id = e.id
WHERE
	rs.nome = 'Twitter';
