INSERT INTO enderecos 
	(rua, pais, cidade)
VALUES
	('Avenida Higienópolis', 'Brasil', 'Londrina');

INSERT INTO enderecos 
	(rua, pais, cidade)
VALUES
	('Avenida Paulista', 'Brasil', 'São Paulo');
	
INSERT INTO enderecos 
	(rua, pais, cidade)
VALUES
	('Rua Marcelino Champagnat', 'Brasil', 'Curitiba');
	
INSERT INTO usuarios 
	(nome, email, senha, endereco_id)
VALUES
	('Cauan', 'cauan@exemple.com', '1234', (SELECT id FROM enderecos WHERE cidade = 'São Paulo'));
	
INSERT INTO usuarios 
	(nome, email, senha, endereco_id)
VALUES
	('Chrystian', 'chrystian@exemple.com', '4321', (SELECT id FROM enderecos WHERE cidade = 'Curitiba'));

INSERT INTO usuarios 
	(nome, email, senha, endereco_id)
VALUES
	('Matheus', 'matheus@exemple.com', '3214', (SELECT id FROM enderecos WHERE cidade = 'Londrina'));
	
INSERT INTO redes_sociais 
	(nome)
VALUES
	('youtube'),
	('Twitter'),
	('Instagram'),
	('Facebook'),
	('TikTok');
	
INSERT INTO usuario_rede_sociais 
	(usuario_id, rede_social_id)
VALUES
	((SELECT id FROM usuarios WHERE nome = 'Cauan'), (SELECT id FROM redes_sociais WHERE nome = 'youtube')) ,    
	((SELECT id FROM usuarios WHERE nome = 'Chrystian'), (SELECT id FROM redes_sociais WHERE nome = 'youtube')), 
	((SELECT id FROM usuarios WHERE nome = 'Matheus'), (SELECT id FROM redes_sociais WHERE nome = 'youtube')),
	((SELECT id FROM usuarios WHERE nome = 'Chrystian'), (SELECT id FROM redes_sociais WHERE nome = 'Twitter')), 
	((SELECT id FROM usuarios WHERE nome =  'Cauan'), (SELECT id FROM redes_sociais WHERE nome = 'Twitter')),
	((SELECT id FROM usuarios WHERE nome = 'Matheus'), (SELECT id FROM redes_sociais WHERE nome = 'Instagram')), 
	((SELECT id FROM usuarios WHERE nome = 'Matheus'), (SELECT id FROM redes_sociais WHERE nome = 'Facebook')),
	((SELECT id FROM usuarios WHERE nome = 'Chrystian'), (SELECT id FROM redes_sociais WHERE nome =  'Instagram')),
	((SELECT id FROM usuarios WHERE nome =  'Cauan'), (SELECT id FROM redes_sociais WHERE nome = 'TikTok'));




	
